/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.consumo.rest.modelo;

import java.io.Serializable;

/**
 *
 * @author Christian Santillan
 */
public class ResponseHeader implements Serializable {
    private String origen;
    private String localizacion;
    private String unicidad;
    private String filler;
    private String idioma;
    private String sesion;
    private String ip;
    private String idCliente;
    private String tipoIdCliente;
    private Documento documento;
    private String respuesta;

    /**
     * @return the origen
     */
    public String getOrigen() {
        return origen;
    }

    /**
     * @param origen the origen to set
     */
    public void setOrigen(String origen) {
        this.origen = origen;
    }

    /**
     * @return the localizacion
     */
    public String getLocalizacion() {
        return localizacion;
    }

    /**
     * @param localizacion the localizacion to set
     */
    public void setLocalizacion(String localizacion) {
        this.localizacion = localizacion;
    }

    /**
     * @return the unicidad
     */
    public String getUnicidad() {
        return unicidad;
    }

    /**
     * @param unicidad the unicidad to set
     */
    public void setUnicidad(String unicidad) {
        this.unicidad = unicidad;
    }

    /**
     * @return the filler
     */
    public String getFiller() {
        return filler;
    }

    /**
     * @param filler the filler to set
     */
    public void setFiller(String filler) {
        this.filler = filler;
    }

    /**
     * @return the idioma
     */
    public String getIdioma() {
        return idioma;
    }

    /**
     * @param idioma the idioma to set
     */
    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    /**
     * @return the sesion
     */
    public String getSesion() {
        return sesion;
    }

    /**
     * @param sesion the sesion to set
     */
    public void setSesion(String sesion) {
        this.sesion = sesion;
    }

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return the idCliente
     */
    public String getIdCliente() {
        return idCliente;
    }

    /**
     * @param idCliente the idCliente to set
     */
    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    /**
     * @return the tipoIdCliente
     */
    public String getTipoIdCliente() {
        return tipoIdCliente;
    }

    /**
     * @param tipoIdCliente the tipoIdCliente to set
     */
    public void setTipoIdCliente(String tipoIdCliente) {
        this.tipoIdCliente = tipoIdCliente;
    }

    /**
     * @return the documento
     */
    public Documento getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    /**
     * @return the respuesta
     */
    public String getRespuesta() {
        return respuesta;
    }

    /**
     * @param respuesta the respuesta to set
     */
    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }
    
 
   }
