/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.consumo.rest.modelo;

import java.io.Serializable;

/**
 *
 * @author Christian Santillan
 */
public class Documento implements Serializable {
    private String fechaContabilidad;
    private String documento;

    /**
     * @return the fechaContabilidad
     */
    public String getFechaContabilidad() {
        return fechaContabilidad;
    }

    /**
     * @param fechaContabilidad the fechaContabilidad to set
     */
    public void setFechaContabilidad(String fechaContabilidad) {
        this.fechaContabilidad = fechaContabilidad;
    }

    /**
     * @return the documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }
    
}
