/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.consumo.rest.modelo;

import java.io.Serializable;

/**
 *
 * @author Christian Santillan
 */
public class CuentaTo implements Serializable {

    private Response response;
    
    private ResponseBody responseBody;

    /**
     * @return the response
     */
    public Response getResponse() {
        return response;
    }

    /**
     * @param response the response to set
     */
    public void setResponse(Response response) {
        this.response = response;
    }

    /**
     * @return the responseBody
     */
    public ResponseBody getResponseBody() {
        return responseBody;
    }

    /**
     * @param responseBody the responseBody to set
     */
    public void setResponseBody(ResponseBody responseBody) {
        this.responseBody = responseBody;
    }
}
