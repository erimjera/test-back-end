/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.consumo.rest.servicio.web;

import com.consumo.rest.modelo.CuentaTo;
import com.consumo.rest.servicio.CuentaServicio;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Christian Santillan
 */
@Path("cuenta")
@Consumes(MediaType.APPLICATION_JSON)
public class CuentaWs {

    @Inject
    private CuentaServicio cuentaServicio;

    @GET
    @Path("obtenerCuentasActivas")
    @Produces(MediaType.APPLICATION_JSON)
    public CuentaTo obtenerCuentasActivas() {
        CuentaTo respuesta = cuentaServicio.devolverActivas();
        return respuesta;

    }

}
