/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.consumo.rest.servicio;

import com.consumo.rest.modelo.CuentaTo;
import com.consumo.rest.modelo.Response;
import com.consumo.rest.modelo.ResponseBody;
import com.consumo.rest.modelo.ResponseHeader;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.stream.Collectors;
import javax.ejb.Stateless;

/**
 *
 * @author Christian Santillan
 */
@Stateless
public class CuentaServicio {

    private static final String ESTADO_CUENTA = "ACTIVO";
    private static final String URL_ORIGEN = "https://run.mocky.io/v3/b886346f-4710-49e2-854f-3f8b87022732";

    public CuentaTo devolverActivas() {
        CuentaTo cuentaEntrada = devolverPeticionGet();
        CuentaTo cuenta = new CuentaTo();
        if (cuentaEntrada != null) {
            cuenta.setResponseBody(new ResponseBody());
            cuenta.getResponseBody().setListaCuentas(cuentaEntrada.getResponseBody().getListaCuentas().stream().filter(cta -> cta.getEstado().equals(ESTADO_CUENTA)).collect(Collectors.toList()));
        } else {
            cuenta.setResponse(new Response());
            cuenta.getResponse().setResponseHeader(new ResponseHeader());
            cuenta.getResponse().getResponseHeader().setRespuesta("Error al consumir el origen.");

        }
        return cuenta;
    }

    private CuentaTo devolverPeticionGet() {
        CuentaTo cuenta = new CuentaTo();
        try {

            URL url = new URL(URL_ORIGEN);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
            StringBuilder cadenaJson = new StringBuilder();
            while ((output = br.readLine()) != null) {
                cadenaJson.append(output);
            }
            conn.disconnect();
            Gson gson = new Gson();
            CuentaTo cta = gson.fromJson(cadenaJson.toString(), CuentaTo.class);
            return cta;
        } catch (IOException e) {
        }

        return cuenta;
    }
}
